@extends('layout')
@section('content')
		@if(session()->has('message'))
            <div class="alert alert-success">
            {{ session()->get('message') }}
        	</div>
        @endif
		<div class="container">
        <div class="mb-3 mt-10">
		<label class="form-label" for="inputName"></label>
		<label class="form-label" for="inputName"></label>
        <form role="form" method="post" action="{{ route('parsedata') }}" id="" enctype="multipart/form-data">
        <input type="file" name="file" accept=".pdf">
        @csrf
        <input type="submit" value="Submit" class="btn btn-primary">
        </form>
        </div>
        </div>
	 <h2>Converted Text from PDF:</h2>
    <pre>{{ $text }}</pre>
@endsection
		