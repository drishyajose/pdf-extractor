<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\PdfToText\Pdf;

class ParseController extends Controller
{
	/**
     * Parse the file.
    */
    public function index(Request $request) 
	{
		$text = null;
		if ($request->getMethod() == 'POST') {
		//dd($request->all());
			if($request->hasFile('file')){
                $file = $request->file('file');
				$filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $fileSize = $file->getSize();
                $mimeType = $file->getMimeType();
                $valid_extension = array("pdf",);
                $maxFileSize = 2097152;//2MB 
                if(!(in_array(strtolower($extension),$valid_extension)))
                    return redirect()->back()->with('message', 'Choose a PDF file !');
				else if($fileSize > $maxFileSize)
                    return redirect()->back()->with('message', 'Max Upload size 2MB !');
			 $location = 'uploads';
			 $file->move($location,$filename);
			 $filePath = public_path($location."/".$filename);
			//  $text = Pdf::getText($filePath)
			 $text = \Spatie\PdfToText\Pdf::getText($filePath, 'C:\Program Files\Git\mingw64\bin\pdftotext.exe');
			}
	}
	return view('index', compact('text'));
	}
}
